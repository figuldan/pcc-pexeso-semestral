#pragma once

/**
 * Maximum due to the limitation of alphabet size
 */
#define MAX_PAIR_SIZE 26

#include <string>
#include <vector>

enum PlayerOptions {
    SINGLE_PLAYER,
    MULTI_PLAYER,
    INVALID // some arbitrary value so we can loop till correct user input is present
};

struct Card {
    bool collected = false;
    bool viewed = false;
    char value{};
};

struct Player {
public:
    int order;
    int score = 0;
    explicit Player(const int &order);
};

class Pexeso {
    int width{};
    int height{};
    int table_size;
    Card** grid{};
    std::vector<Player> players;

public:

    PlayerOptions player_options;

    Pexeso(int &table_size, PlayerOptions &player_options);

    ~Pexeso();

    /**
     * Calls other private function for the game initialization
     */
    void prepare_game();

    void start_single_player();

    void start_multi_player();

private:
    /**
     * Sets the width and height of the Pexeso from the
     * initial table size so it is as most symmetrical as possible
     */
    void calculate_grid_dimension();

    void create_grid();

    void create_players();

    void print_grid();

    /**
     * Checks whether all the cards were already collected by the players
     * @return true if collected, false if not
     */
    bool all_cards_collected();

    /**
     * Takes a player turn which calls for user input at least twice
     * to select their card
     * @return true if cards matched, false if not
     */
    bool take_player_turn();

    /**
     * @param order string that should be shown in the terminal as an order
     * @return selected card
     */
    Card * select_card(const std::string& order);
};