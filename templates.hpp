/**
 * File with all the templates that are shown in the terminal
 */

#include <string>

const std::string title = R"(
     .----------------. .----------------. .----------------. .----------------. .----------------. .----------------.
    | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. |
    | |   ______     | | |  _________   | | |  ____  ____  | | |  _________   | | |    _______   | | |     ____     | |
    | |  |_   __ \   | | | |_   ___  |  | | | |_  _||_  _| | | | |_   ___  |  | | |   /  ___  |  | | |   .'    `.   | |
    | |    | |__) |  | | |   | |_  \_|  | | |   \ \  / /   | | |   | |_  \_|  | | |  |  (__ \_|  | | |  /  .--.  \  | |
    | |    |  ___/   | | |   |  _|  _   | | |    > `' <    | | |   |  _|  _   | | |   '.___`-.   | | |  | |    | |  | |
    | |   _| |_      | | |  _| |___/ |  | | |  _/ /'`\ \_  | | |  _| |___/ |  | | |  |`\____) |  | | |  \  `--'  /  | |
    | |  |_____|     | | | |_________|  | | | |____||____| | | | |_________|  | | |  |_______.'  | | |   `.____.'   | |
    | |              | | |              | | |              | | |              | | |              | | |              | |
    | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' |
     '----------------' '----------------' '----------------' '----------------' '----------------' '----------------'
)";

const std::string main_menu = R"(
                                              Welcome to the game of Pexeso!

                                            Choose from the following main_menu:

                                                    S - Start game

                                                    O - Options

                                                    X - Exit
)";

const std::string options_warning = R"(
                                                Choose game options first!
)";

const std::string pair_input_number_warning = R"(
                                                Please enter a number:
)";

const std::string pair_input_limit_warning = R"(
                                        Please enter a number from the valid range:
)";

const std::string options_title = R"(
                                            Choose from the following settings:
)";

const std::string player_options = R"(
                                                    1 - Single player

                                                    2 - Multi player (2 player)
)";

const std::string pair_option = R"(
                                    How many pairs would you like to generate (1-26)?
)";

const std::string help_menu = R"(
                                                    How to play:

                                After each argument press Enter to select your choice.

                                1. Go to Options from the main menu and select game mode

                                    - Single player: Select cards until you get them all
        - Multi player: You play with other player, for each pair you get a point, player with the most points wins
                To select number of pairs select from range 0 - 26 pair otherwise it is considered as invalid
                                        (limitation of alphabet characters)

                                                2. Start the game

                        Grid will be shown with one side with numbers and letters from the other
                    To select your desired card, enter the argument in the format [<letter>][<number>]
            For example, A1 selects from row A and column 1. Any other argument will be considered as invalid.
            After playing the game, you will be returned to game menu. You may play again with your previous setup
                    or you may go to options again to select different game mode / configuration.

)";