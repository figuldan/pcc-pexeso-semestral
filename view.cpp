/**
 * Value that resets the terminal screen
 */
#define ANSI_CLEAR  "\x1B[2J\x1B[H"

#include <iostream>

#include "view.hpp"
#include "templates.hpp"
#include "pexeso.hpp"

char get_input()
{
    char choice;
    std::cin >> choice;
    return choice;
}

void clear_and_display_title()
{
    std::cout << ANSI_CLEAR;
    std::cout << title;
}

void display_help_menu()
{
    clear_and_display_title();
    std::cout << help_menu;
}

bool parse_integer_input(std::string& input, int& output) {
    try{
        output = std::stoi(input);
    } catch (std::invalid_argument const&) {
        return false;
    }
    if (output <= 0 || output > MAX_PAIR_SIZE) {
        std::cout << pair_input_limit_warning << std::endl;
        return false;
    }

    return true;
}

Pexeso* render_options()
{
    PlayerOptions options;
    clear_and_display_title();
    std::cout << options_title;
    std::cout << player_options;

    do {
        switch(get_input()) {
            case '1':
                options = SINGLE_PLAYER;
                break;
            case '2':
                options = MULTI_PLAYER;
                break;
            default:
                options = INVALID;
                clear_and_display_title();
                std::cout << options_title;
                std::cout << player_options;
                break;
        }
    } while (options == INVALID);

    clear_and_display_title();
    std::cout << options_title;

    int table_size;
    std::string user_input;
    std::cout << pair_option;
    std::cin >> user_input;

    while (!parse_integer_input(user_input, table_size))
    {
        std::cout << pair_input_number_warning << std::endl;
        std::cin >> user_input;
    }

    clear_and_display_title();
    std::cout << main_menu << std::endl;
    return new Pexeso{table_size, options};
}

void render_main_menu()
{
    Pexeso* pexeso = nullptr;
    char choice;
    bool was_warned = false;

    do {
        clear_and_display_title();
        std::cout << main_menu << std::endl;
        if (was_warned) {
            std::cout << options_warning;
        }
        choice = get_input();
        switch(choice) {
            case 's':
            case 'S':
                if (pexeso) {
                    was_warned = false;
                    pexeso->prepare_game();
                    if (pexeso->player_options == SINGLE_PLAYER) {
                        pexeso->start_single_player();
                    } else {
                        pexeso->start_multi_player();
                    }
                } else {
                    was_warned = true;
                }
                break;
            case 'o':
            case 'O':
                was_warned = false;
                delete pexeso;
                pexeso = render_options();
                break;
            default:
                break;
        }
    } while (choice != 'X' && choice != 'x');
    delete pexeso;
    std::cout << ANSI_CLEAR;
}