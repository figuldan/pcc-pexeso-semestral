# PCC Pexeso Semestral

<!-- TOC -->
* [PCC Pexeso Semestral](#pcc-pexeso-semestral)
  * [Task](#task)
  * [How to run](#how-to-run)
    * [Build](#build)
      * [Unix](#unix)
      * [Windows](#windows)
    * [Run](#run)
  * [FAQ](#faq)
<!-- TOC -->

## Task
The assignment for the term paper was the creation of a play. As a topic I chose Pexeso - a game where the player uncovers
cards one by one and searches for their second pair until he finds them all. The game provides two modes of play:

1. Single-player - the player plays alone and searches for the cards until he finds all of them.
2. Multi-player - two players play against each other and the player with the most pairs wins.
   Players alternate turns, except when a player finds a pair, in which case they play again

## How to run

### Build
#### Unix
In this directory, first create another directory and change to that directory :
```shell
mkdir cmake-build-debug
cd cmake-build-debug
```
Run CMake from this directory upwards and run Make to build the game:
```shell
cmake ..
make
```
Run your game or run it with the `--help` command just in case you have forgotten how to play it:
```shell
./Pexeso
```
#### Windows
I highly recommend using WSL 2 to use the solution mentioned above, otherwise build it in Visual Studio
and run it from there.

### Run

After running the game, player is put in the welcome screen. To navigate, write the following option into the 
terminal and **press ENTER**. You need to do this for every command (see [FAQ](#FAQ)).

```
     .----------------. .----------------. .----------------. .----------------. .----------------. .----------------.
    | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. |
    | |   ______     | | |  _________   | | |  ____  ____  | | |  _________   | | |    _______   | | |     ____     | |
    | |  |_   __ \   | | | |_   ___  |  | | | |_  _||_  _| | | | |_   ___  |  | | |   /  ___  |  | | |   .'    `.   | |
    | |    | |__) |  | | |   | |_  \_|  | | |   \ \  / /   | | |   | |_  \_|  | | |  |  (__ \_|  | | |  /  .--.  \  | |
    | |    |  ___/   | | |   |  _|  _   | | |    > `' <    | | |   |  _|  _   | | |   '.___`-.   | | |  | |    | |  | |
    | |   _| |_      | | |  _| |___/ |  | | |  _/ /'`\ \_  | | |  _| |___/ |  | | |  |`\____) |  | | |  \  `--'  /  | |
    | |  |_____|     | | | |_________|  | | | |____||____| | | | |_________|  | | |  |_______.'  | | |   `.____.'   | |
    | |              | | |              | | |              | | |              | | |              | | |              | |
    | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' |
     '----------------' '----------------' '----------------' '----------------' '----------------' '----------------'
     
                                               Welcome to the game of Pexeso!

                                            Choose from the following main_menu:

                                                    S - Start game

                                                    O - Options

                                                    X - Exit
S
                                                Choose game options first!
o                        
```

Go to the options settings and select game-mode you want to play:

```
                                                    1 - Single player

                                                    2 - Multi player (2 player)
1
```
Select number of pairs you want from the given range:

```
                                      How many pairs would you like to generate (1-26)?
20
```

And play:

```
   1   2   3   4   5
  --- --- --- --- ---
A| X | X | X | X | X |A
  --- --- --- --- ---
B| X | X | X | X | X |B
  --- --- --- --- ---
C| X | X | X | X | X |C
  --- --- --- --- ---
D| X | X | X | X | X |D
  --- --- --- --- ---
E| X | X | X | X | X |E
  --- --- --- --- ---
F| X | X | X | X | X |F
  --- --- --- --- ---
G| X | X | X | X | X |G
  --- --- --- --- ---
H| X | X | X | X | X |H
  --- --- --- --- ---
   1   2   3   4   5

Enter a first field (e.g. B2): A1

   1   2   3   4   5
  --- --- --- --- ---
A| t | X | X | X | X |A
  --- --- --- --- ---
B| X | X | X | X | X |B
  --- --- --- --- ---
C| X | X | X | X | X |C
  --- --- --- --- ---
D| X | X | X | X | X |D
  --- --- --- --- ---
E| X | X | X | X | X |E
  --- --- --- --- ---
F| X | X | X | X | X |F
  --- --- --- --- ---
G| X | X | X | X | X |G
  --- --- --- --- ---
H| X | X | X | X | X |H
  --- --- --- --- ---
   1   2   3   4   5

Enter a second field (e.g. B2): A1

   1   2   3   4   5
  --- --- --- --- ---
A| X | X | X | X | q |A
  --- --- --- --- ---
B| X | X | X | X | X |B
  --- --- --- --- ---
C| X | X | X | X | X |C
  --- --- --- --- ---
D| X | X | X | X | X |D
  --- --- --- --- ---
E| X | X | X | X | X |E
  --- --- --- --- ---
F| X | X | X | X | X |F
  --- --- --- --- ---
G| X | X | X | X | X |G
  --- --- --- --- ---
H| X | g | X | X | X |H
  --- --- --- --- ---
   1   2   3   4   5

Try again!
Enter a first field (e.g. B2):
```

Until you are finished playing!

```
   1   2   3   4   5
  --- --- --- --- ---
A| t |   |   |   |   |A
  --- --- --- --- ---
B|   |   |   |   |   |B
  --- --- --- --- ---
C|   |   |   |   |   |C
  --- --- --- --- ---
D|   |   |   |   |   |D
  --- --- --- --- ---
E|   |   |   |   |   |E
  --- --- --- --- ---
F|   |   |   |   |   |F
  --- --- --- --- ---
G|   |   |   |   |   |G
  --- --- --- --- ---
H|   | t |   |   |   |H
  --- --- --- --- ---
   1   2   3   4   5
   
Good job!
Congratulations! You've collected all the cards. You win!

Press Enter to continue . . .
```

Which returns you to the main screen.

## Testing

I have tested this application by trying to mess with certain inputs of the program, tried different boards
and checked the whole application with **Valgrind** for memory leaks. 

## FAQ

**Q: Why do I have to press enter for every command I write?**

**A:** I hate it myself when I do not have the option to confirm the decision I want to make and by accidentally miss-clicking
you have no way to go back, therefore I implemented it the way I would personally like to use it.
