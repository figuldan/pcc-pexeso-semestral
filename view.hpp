#pragma once

/**
 * Renders the first menu that appears at the start
 * and waits for user-input
 */
void render_main_menu();

/**
 * Renders the help menu for the user when programme is run with
 * --help argument
 */
void display_help_menu();

/**
 * Takes the input from the terminal and tries to convert it
 * from the string value to integer value. Also checks
 * whether the input is within the desired range
 *
 * @param input string terminal input
 * @param output  integer terminal output
 * @return true if successfully parsed
 */
bool parse_integer_input(std::string& input, int& output);