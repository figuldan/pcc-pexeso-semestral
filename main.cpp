#include <cstdlib>
#include <iostream>
#include "view.hpp"

bool is_set_help(int &argc, char *argv[])
{
    for (int i = 0; i < argc; ++i) {
        std::string arg = argv[i];
        if (arg == "--help") {
            return true;
        }
    }
    return false;
}

int main(int argc, char *argv[])
{
    is_set_help(argc, argv) ? display_help_menu() : render_main_menu();
    return EXIT_SUCCESS;
}

