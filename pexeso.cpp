#include "pexeso.hpp"
#include <cmath>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <random>
#include <iomanip>

Pexeso::Pexeso(int &table_size, PlayerOptions &player_options):
    table_size(table_size),
    player_options(player_options){
}

void Pexeso::prepare_game() {
    calculate_grid_dimension();
    create_grid();
    create_players();
    print_grid();
}

void Pexeso::start_single_player()
{
    while (!all_cards_collected()) {
        take_player_turn();
    }

    std::cout << "Congratulations! You've collected all the cards. You win!\n" << std::endl;
    std::cout << "Press Enter to continue . . .";

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
}


void Pexeso::start_multi_player()
{
    while (!all_cards_collected())
    {
        for (Player &player : players)
        {
            std::cout << "Player " << player.order << " takes turn!" << std::endl;
            while (take_player_turn())
            {
                player.score++;
                if (all_cards_collected())
                {
                    break;
                }
                std::cout << "Player " << player.order << " takes turn again!" << std::endl;
            }
            if (all_cards_collected())
            {
                break;
            }
        }
    }
    auto max_player = std::max_element(players.begin(), players.end(),
                                       [](const Player &a, const Player &b) {
                                          return a.score < b.score;
                                      });

    if (max_player != players.end())
    {
        Player res = *max_player;
        int max_score = res.score;

        std::cout << "Player " << res.order << " wins with score of : " << max_score << std::endl;
    } else {
        std::cout << "Draw!" << std::endl;
    }
    std::cout << "Press Enter to continue . . .";

    // Checks for user input twice to clear the user input and wait for Enter
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
}

Card * Pexeso::select_card(const std::string& order)
{
    Card* card;

    while (true) {
        std::string card_location;
        std::cout << "Enter a " << order <<  " field (e.g. B2): ";
        std::cin >> card_location;

        int row = card_location[0] - 'A';
        int col = card_location[1] - '1';

        if (row < 0 || row >= height || col < 0 || col >= width) {
            std::cout << "Invalid argument, please try again." << std::endl;
            continue;
        }

        card = &grid[row][col];

        if (card == nullptr) {
            std::cout << "Invalid grid coordinates, please try again." << std::endl;
            continue;
        }

        if (card->collected) {
            std::cout << "Card already collected, please select another card" << std::endl;
            continue;
        }

        card->viewed = true;
        print_grid();

        return card;
    }
}

bool Pexeso::take_player_turn()
{
    Card *first_card = select_card("first");
    Card *second_card = select_card("second");

    first_card->viewed = false;
    second_card->viewed = false;

    if (first_card->value == second_card->value)
    {
        first_card->collected = true;
        second_card->collected = true;
        std::cout << "Good job!" << std::endl;
        return true;
    } else
    {
        std::cout << "Try again!" << std::endl;
        return false;
    }

}

bool Pexeso::all_cards_collected()
{
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            if (!grid[y][x].collected)
            {
                return false;
            }
        }
    }
    return true;
}

void Pexeso::create_players()
{
    players.emplace_back(1);

    switch (player_options)
    {
        case SINGLE_PLAYER:
            break;
        case MULTI_PLAYER:
            players.emplace_back(2);
            break;
        case INVALID:
            throw std::invalid_argument("Not a valid option!");
    }
}

void Pexeso::calculate_grid_dimension()
{
    int final_size = table_size * 2;

    int sqrt_input = static_cast<int>(sqrt(final_size));

    for (int value = sqrt_input; value >= 1; --value)
    {
        if (final_size % value == 0) {
            width = value;
            height = final_size / value;
            return;
        }
    }
}

void Pexeso::create_grid()
{
    int card_count = (width * height) / 2;
    std::vector<Card> cards;

    for (int i = 0; i < card_count; i++) {
        for (int j = 0; j < 2; ++j) {
            cards.push_back(Card{false, false, (char) (97 + i)});
        }
    }

    // Shuffles the cards in the random order
    std::default_random_engine rng(std::random_device{}());
    std::shuffle(cards.begin(), cards.end(), rng);

    grid = new Card*[height];
    for (int i = 0; i < height; ++i) {
        grid[i] = new Card[width];
    }

    int index = 0;
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            grid[y][x] = cards[index++];
        }
    }

}

void Pexeso::print_grid()
{
    // This function is just a long complex logic of printing
    // the grid correctly
    std::stringstream buffer;

    buffer << std::endl;
    buffer << " ";

    int i = 1, j;
    for(j = 0; j <= 4*width; j++)
    {
        if(j % 4 == 2)
        {
            buffer << i++;
        }
        else
        {
            buffer << " ";
        }
    }
    buffer << std::endl;

    for(i = 0; i <= 2*height; i++)
    {
        if(i % 2 != 0)
        {
            buffer << (char) (i/2 +'A');
        }
        for(j = 0; j <= 2*width; j++)
        {
            if(i % 2 == 0)
            {
                if(j == 0)
                {
                    buffer << " ";
                }
                if(j % 2 == 0)
                {
                    buffer << " ";
                }
                else
                {
                    buffer << "---";
                }
            }
            else
            {
                if(j % 2 == 0)
                {
                    buffer << "|";
                }
                else {
                    Card card = grid[i / 2][j / 2];
                    buffer << " ";
                    if (card.collected)
                    {
                        buffer << " ";
                    }
                    else if(!card.viewed) {
                        buffer << "X";
                    }
                    else
                    {
                        buffer << card.value;
                    }
                    buffer << " ";
                }
            }
        }
        if(i % 2 != 0)
        {
            buffer << (char)(i / 2 +'A');
        }
        buffer << std::endl;
    }
    buffer << " ";
    for(j = 0, i = 1; j <= 4*width; j++)
    {
        if(j % 4 == 2)
        {
            buffer << i++;
        }
        else
        {
            buffer << " ";
        }
    }
    buffer << std::endl;
    std::cout << buffer.str() << std::endl;
}

Pexeso::~Pexeso() {
    for (int i = 0; i < height; ++i) {
        delete[] grid[i];
    }
    delete[] grid;
}

Player::Player(const int &order):
    order(order){
}
